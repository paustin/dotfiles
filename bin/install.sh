#!/bin/bash
set -e

MIRROR="https://gentoo.osuosl.org/"
STAGE3=$(wget --quiet "$MIRROR/releases/amd64/autobuilds/latest-stage3-amd64.txt" -O-| tail -n 1 | cut -d " " -f 1 | awk -F/ '{ print $2}')
STAGE3_URL="https://gentoo.osuosl.org/releases/amd64/autobuilds/current-stage3-amd64/$STAGE3"

/etc/init.d/lvm start
ntpd -q -g

parted --script /dev/sda \
unit mib \
mklabel gpt \
mkpart primary 1 3 \
name 1 grub \
set 1 bios_grub on \
mkpart primary fat32 3 515 \
name 2 boot \
set 2 BOOT on \
mkpart primary 515 8707 \
name 3 swap \
-- mkpart primary 8707 -1 \
name 4 lvm \
set 4 lvm on \
quit

mkswap /dev/sda3
swapon /dev/sda3
mkfs.vfat -F32 /dev/sda2

cryptsetup luksFormat -c aes-cbc-essiv:sha256 /dev/sda4
cryptsetup luksOpen /dev/sda4 lvm
lvm pvcreate /dev/mapper/lvm 
vgcreate vg0 /dev/mapper/lvm 
lvcreate -l 100%FREE -n root vg0
mkfs.ext4 /dev/mapper/vg0-root
mount /dev/mapper/vg0-root /mnt/gentoo

cd /mnt/gentoo
wget $STAGE3_URL
tar xpvf stage3-*.tar.* --xattrs-include='*.*' --numeric-owner
cd -

mirrorselect -i -o >> /mnt/gentoo/etc/portage/make.conf
mkdir --parents /mnt/gentoo/etc/portage/repos.conf
cp /mnt/gentoo/usr/share/portage/config/repos.conf /mnt/gentoo/etc/portage/repos.conf/gentoo.conf

cp --dereference /etc/resolv.conf /mnt/gentoo/etc/
mount --types proc /proc /mnt/gentoo/proc 
mount --rbind /sys /mnt/gentoo/sys 
mount --make-rslave /mnt/gentoo/sys 
mount --rbind /dev /mnt/gentoo/dev 
mount --make-rslave /mnt/gentoo/dev 

test -L /dev/shm && rm /dev/shm && mkdir /dev/shm 
mount -t tmpfs -o nosuid,nodev,noexec shm /dev/shm
chmod 1777 /dev/shm

unzip ../../dotfiles-*.zip -d /mnt/gentoo/root/
cp /mnt/gentoo/root/dotfiles-*/bin/step2.sh /mnt/gentoo/
chroot /mnt/gentoo /bin/bash ./step2.sh
