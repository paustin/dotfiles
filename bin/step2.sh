#!/bin/bash
source /etc/profile
export PS1="(chroot) $PS1"
set -e

SDA2UUID=$(blkid | grep /dev/sda2 | awk 'match($0,/"[^UUID="]*"/) {print substr($0,RSTART+1,RLENGTH-2)}')
SDA3UUID=$(blkid | grep /dev/sda3 | awk 'match($0,/"[^UUID="]*"/) {print substr($0,RSTART+1,RLENGTH-2)}')
SDA4UUID=$(blkid | grep /dev/sda4 | awk 'match($0,/"[^UUID="]*"/) {print substr($0,RSTART+1,RLENGTH-2)}')
VG0ROOTUUID=$(blkid | grep /dev/mapper/vg0-root | awk 'match($0,/"[^UUID="]*"/) {print substr($0,RSTART+1,RLENGTH-2)}')

mount /dev/sda2 /boot

cp -rf /root/dotfiles-*/etc/portage/* /etc/portage/
cp -rf /root/dotfiles-*/etc/timezone /etc/
cp -rf /root/dotfiles-*/etc/locale.gen /etc/

emerge-webrsync

emerge --config sys-libs/timezone-data 
locale-gen
eselect locale set en_US.utf8
env-update && source /etc/profile && export PS1="(chroot) $PS1"

echo "UUID='$SDA2UUID' /boot vfat noauto,noatime 1 2" > /etc/fstab
echo "UUID='$SDA3UUID' none swap sw 0 0" >> /etc/fstab
echo "UUID='$VG0ROOTUUID' / ext4 defaults 0 1" >> /etc/fstab

emerge -uDU @world sys-kernel/gentoo-sources sys-kernel/genkernel sys-fs/cryptsetup sys-apps/pciutils sys-kernel/linux-firmware grub vim net-misc/dhcpcd sys-boot/os-prober app-shells/zsh sudo dev-vcs/git

cp -rf /root/dotfiles-*/usr/src/linux/.config /usr/src/linux/
#genkernel --save-config --install --luks --lvm --makeopts=-j17 --menuconfig all
cd /usr/src/linux
make menuconfig

make -j4 && make modules_install
make install
genkernel --luks --lvm --install initramfs

echo "hostname='gentoo'" > /etc/conf.d/hostname

passwd

groupadd sudo
EDITOR=vim visudo

useradd -m -G users,wheel,sudo,video,audio -s /bin/zsh paustin
passwd paustin

echo "GRUB_CMDLINE_LINUX='dolvm crypt_root=UUID=$SDA4UUID root=/dev/mapper/vg0-root'" >> /etc/default/grub

grub-install /dev/sda
grub-mkconfig -o /boot/grub/grub.cfg
